import bodyParser = require("body-parser");
import { expect } from "chai";
import express = require("express");
import request = require("supertest");
import td = require("testdouble");
import Clock from "../src/lib/Clock";
import Configuration, { BankConfig, BankProvider } from "../src/lib/Configuration";
import TreasuryPrime from "../src/routes/treasury_prime";

const DEFAULT_TREASURY_PRIME_CONFIG = {
    provider: BankProvider.TreasuryPrime,
    baseUrl: "treasury_prime",
    authentication: null,
};

function treasuryPrime(bankConfig: BankConfig = DEFAULT_TREASURY_PRIME_CONFIG, clock: Clock = new Clock()) {
    const testme = new TreasuryPrime(new Configuration(), bankConfig, clock);
    const app = express();
    const router = testme.produceRouter();

    app.use(bodyParser.json());
    app.use(router);

    return request(app);
}

describe("Given treasury prime mock bank", () => {
    const treasuryPrimeWithAuth = (clock: Clock = new Clock()) => treasuryPrime({
        provider: BankProvider.TreasuryPrime,
        baseUrl: "treasury_prime",
        authentication: {
            username: "my-fake-username",
            password: "my-fake-password",
        },
    }, clock);

    describe("When I get details", () => {
        const getAccount = (id) => treasuryPrimeWithAuth()
            .get("/account")
            .auth("my-fake-username", "my-fake-password")
            .query({ account_number: id });

        describe("for account number 123456", () => {

            it("Then 'acct' is prepended to account id", async () => {
                await getAccount("123456")
                    .expect(200)
                    .expect(hasAccountId("acct_123456"));
            });
        });

        describe("using blank account number", () => {
            it("returns empty acccount number error", async () => {
                await getAccount("")
                    .expect(200, {error: "Empty string account_number: "});
            });
        });

        describe("using wrong paremeter name", () => {
            it("returns missing parameter error", async () => {
                await treasuryPrimeWithAuth()
                    .get("/account")
                    .auth("my-fake-username", "my-fake-password")
                    .query({ account_number_blah: "" })
                    .expect(200, {
                        error: "Invalid parameter name passed in: account_number_blah",
                    });
            });
        });
    });

    describe("Given an account has no transactions", () => {

        describe("When I get account details", () => {
            const getAccount = () => treasuryPrimeWithAuth()
                .get("/account/acct_123")
                .auth("my-fake-username", "my-fake-password");

            it("initial balance is $10,000,000.00", async () => {
                await getAccount()
                    .expect(200)
                    .expect(hasBalance("10000000.00"));
            });

            it("account number has 'acct' removed", async () => {
                await getAccount()
                    .expect(200)
                    .expect(hasAccountNumber("123"));
            });
        });
    });

    describe("When I get account details for account 'bad-id'", () => {
        const getBlankAccount = () => treasuryPrimeWithAuth()
            .get("/account/bad-id")
            .auth("my-fake-username", "my-fake-password");

        it("error identifies missing id", async () => {
            await getBlankAccount()
                .expect(404)
                .expect(error("Record with id bad-id not found."));
        });
    });
    describe("Given the current datetime is 2020-02-04 10:10:10Z", () => {

        const current = new Date("2020-02-04 10:10:10Z");
        const fakeClock = td.object<Clock>();
        td.when(fakeClock.now()).thenReturn(current);
        const app = treasuryPrime(null, fakeClock);

        describe("When I create a $50.12 transfer from account 123 to account 456 with metadata 'meta1'", () => {
            const bookRequest = () => app
                .post("/book")
                .send(bookTransfer("acct_123", "acct_456", "50.12", "meta1"));

            it("it is succesfully created", async () => {
                await bookRequest()
                    .expect(200);
            });

            it("create date is 2020-02-04 10:10:10Z", async () => {
                await bookRequest()
                    .expect((res) => expect(res.body.created_at).to.equal(current.toISOString()));
            });

            it("update date is 2020-02-04 10:10:10Z", async () => {
                await bookRequest()
                    .expect((res) => expect(res.body.updated_at).to.equal(current.toISOString()));
            });

            it("amount is $50.12", async () => {
                await bookRequest()
                    .expect((res) => expect(res.body.amount).to.equal("50.12"));
            });

            it("metadata is 'meta1'", async () => {
                await bookRequest()
                    .expect((res) => expect(res.body.userdata.metadata).to.equal("meta1"));
            });
        });
    });

    describe("Given I transfer $10 from account 555 to account 666", () => {

        const app = treasuryPrimeWithAuth();
        before((done) => {
            app
                .post("/book")
                .auth("my-fake-username", "my-fake-password")
                .send(bookTransfer("acct_555", "acct_666", "10.00", "meta1"))
                .end(done);
        });

        describe("When I get account details for account 555", () => {
            it("balance is $9,999,990", async () => {
                await app
                    .get("/account/acct_555")
                    .auth("my-fake-username", "my-fake-password")
                    .expect(hasBalance("9999990.00"));
            });
        });

        describe("When I get account details for account 666", () => {
            it("balance is $10,000,010", async () => {
                await app
                    .get("/account/acct_666")
                    .auth("my-fake-username", "my-fake-password")
                    .expect(hasBalance("10000010.00"));
            });
        });
    });

    describe("Given an existing account with no funds", () => {

        const app = treasuryPrimeWithAuth();
        before((done) => {
            app
                .post("/book")
                .auth("my-fake-username", "my-fake-password")
                .send(bookTransfer("acct_555", "acct_666", "10000000.00", "meta1"))
                .end(done);
        });

        describe("When I get account details for account 555", () => {
            it("balance is $0", async () => {
                await app
                    .get("/account/acct_555")
                    .auth("my-fake-username", "my-fake-password")
                    .expect(hasBalance("0.00"));
            });
        });
    });

    describe("Given the default page size is 100 and there are 101 book transfers", () => {

        const app = treasuryPrimeWithAuth();

        before(async () => {
            await Promise.all([...Array(101)].map(async () => await seedBookTransfer(app)));
        });

        describe("When I request all book transfers", () => {
            const allBooks = () => app
                .get("/book")
                .auth("my-fake-username", "my-fake-password");

            it("Then only 100 results are returned", async () => {
                await allBooks()
                    .expect(200)
                    .expect(({ body }) => expect(body.data.length).to.equal(100));
            });

            it("And a cursor is returned", async () => {
                await allBooks()
                    .expect(200)
                    .expect(({ body }) => expect(body.next_page).to.exist);
            });
        });
    });

    describe("Given three book transfers created 24hrs apart", () => {

        const firstBookCreatedAt = new Date("2020-02-04 10:10:10Z");
        const secondBookCreatedAt = new Date("2020-02-05 10:10:10Z");
        const thirdBookCreatedAt = new Date("2020-02-06 10:10:10Z");

        const fakeClock = td.object<Clock>();
        td.when(fakeClock.now()).thenReturn(
            firstBookCreatedAt,
            firstBookCreatedAt,
            secondBookCreatedAt,
            secondBookCreatedAt,
            thirdBookCreatedAt,
            thirdBookCreatedAt);

        const app = treasuryPrimeWithAuth(fakeClock);

        let firstBookId: string;
        let secondBookId: string;
        let lastBookId: string;

        before(async () => {
            firstBookId = await seedBookTransfer(app);
            secondBookId = await seedBookTransfer(app);
            lastBookId = await seedBookTransfer(app);
        });

        const pagedRequest = (cursorId) => app
            .get("/book")
            .auth("my-fake-username", "my-fake-password")
            .query({ page_size: "2", page_cursor: cursorId });

        describe("When I get all book requests ", () => {
            it("Then they are sorted by create date descending", async () => {
                await app
                    .get("/book")
                    .auth("my-fake-username", "my-fake-password")
                    .expect(({ body: { data } }) => {
                        expect(data.length).to.equal(3);
                        expect(data[0].id).to.equal(lastBookId);
                        expect(data[1].id).to.equal(secondBookId);
                        expect(data[2].id).to.equal(firstBookId);
                    });
            });

        });

        describe("When I get book requests with page size of 2", () => {
            it("Then the two most recent book transfers are returned", async () => {
                await pagedRequest(null)
                    .expect(({body: { data }}) => {
                        expect(data.length).to.equal(2);
                        expect(data[0].id).to.equal(lastBookId);
                        expect(data[1].id).to.equal(secondBookId);
                    });
            });

            it("Then the cursor is set to the last book id in the page", async () => {
                await pagedRequest(null)
                    .expect(({ body }) => {
                        expect(body.next_page).to.include(secondBookId);
                    });
            });
        });

        describe("When I get the second page of book transfers with page size of 2", () => {
            it("Then the first book transfer is returned", async () => {
                await pagedRequest(secondBookId)
                    .expect(({body: { data }}) => {
                        expect(data.length).to.equal(1);
                        expect(data[0].id).to.equal(firstBookId);
                    });
            });

            it("Then no cursor is defined in results", async () => {
                await pagedRequest(secondBookId)
                    .expect(({ body }) => expect(body.next_page).to.equal(undefined));
            });
        });

        describe("When I get book transfers with from/to date around second book transfer created date", () => {

            const secondBookOnly = () => app
                .get("/book")
                .auth("my-fake-username", "my-fake-password")
                .query({
                    from_date: new Date(firstBookCreatedAt.getTime() + 1000),
                    to_date: new Date(thirdBookCreatedAt.getTime() - 1000),
                });

            it("Then only the second book transfer is returned", async () => {
                await secondBookOnly()
                    .expect(({ body }) => {
                        expect(body.data.length).to.equal(1);
                        expect(body.data[0].id).to.equal(secondBookId);
                    });
            });
        });

        describe("When I get book transfers with from/to date around first and second book transfers created date", () => {
            const secondBookOnly = () => app
                .get("/book")
                .auth("my-fake-username", "my-fake-password")
                .query({
                    from_date: new Date(firstBookCreatedAt.getTime() - 1000),
                    to_date: new Date(thirdBookCreatedAt.getTime() - 1000),
                });

            it("Then only the first and second book transfers are returned", async () => {
                await secondBookOnly()
                    .expect((res) => {
                        expect(res.body.data.length).to.equal(2);
                        expect(res.body.data[0].id).to.equal(secondBookId);
                        expect(res.body.data[1].id).to.equal(firstBookId);
                    });
            });
        });
    });

    describe("When I try to make a request without authentication", () => {
        const makeUnauthenticatedRequest = () => treasuryPrimeWithAuth()
            .get("/account")
            .query({ account_number: "123456" });

        it("Then an http 401 error is returned", async () => {
            await makeUnauthenticatedRequest()
                .expect(401);
        });
    });
});

function hasAccountId(accountId) {
    return (res) => {
        const account = res.body.data[0];
        expect(account.id).to.equal(accountId);
    };
}

function hasAccountNumber(accountNumber) {
    return (res) => {
        const account = res.body;
        expect(account.account_number).to.equal(accountNumber);
    };
}

function hasBalance(balance) {
    return (res) => {
        expect(res.body.current_balance).to.equal(balance);
        expect(res.body.available_balance).to.equal(balance);
    };
}

function error(message) {
    return (res) => {
        expect(res.body.error).to.equal(message);
    };
}

function bookTransfer(from, to, amount, meta) {
    return {
        amount,
        from_account_id: from,
        to_account_id: to,
        userdata: { metadata: meta },
    };
}

async function seedBookTransfer(app): Promise<string> {
    const res = await app
        .post("/book")
        .auth("my-fake-username", "my-fake-password")
        .send(bookTransfer("acct_777", "acct_888", "10.00", "meta1"))
        .expect(200);

    return res.body.id;
}
