export default class Clock {
    public now(): Date {
        return new Date();
    }
}
