import { HttpAuthentication } from "./Authentication";

export enum BankProvider {
    Silvergate,
    Metropolitan,
    TreasuryPrime,
}

export interface BankConfig {
    baseUrl: string;
    authentication: HttpAuthentication | null;
    provider: BankProvider;
}

export default class Configuration {

    public static getDefaultBaseUrlForProvider(provider: BankProvider): string {
        return {
            [BankProvider.Silvergate]: "silvergate",
            [BankProvider.Metropolitan]: "metropolitan",
            [BankProvider.TreasuryPrime]: "treasury_prime",
        }[provider];
    }

    public static getProviderByName(name: string): BankProvider | null {
        const value = {
            silvergate: BankProvider.Silvergate,
            metropolitan: BankProvider.Metropolitan,
            treasury_prime: BankProvider.TreasuryPrime,
        }[name];

        return value ?? null;
    }

    public static getDefaultBanksSetting(): BankConfig[] {
        return [
            {
                provider: BankProvider.Metropolitan,
                baseUrl: "metropolitan",
                authentication: null,
            },
            {
                provider: BankProvider.Silvergate,
                baseUrl: "silvergate",
                authentication: null,
            },
            {
                // Note: the default bank set specifies a "provident" route backed by Treasury Prime
                provider: BankProvider.TreasuryPrime,
                baseUrl: "provident",
                authentication: null,
            },
        ];
    }
    public banks: BankConfig[];
    public initialReserveBalance: number;

    public assertValid() {
        const seenRoutes = new Set();

        for (const bank of this.banks) {
            if (!bank.baseUrl.match(/^[a-z0-9\-_]+$/i)) {
                throw new Error(`Invalid configuration: base URL "${bank.baseUrl}" must only contain letters, numbers, dashes and underscores, and be at least one character long`);
            }

            if (bank.baseUrl === "health") {
                throw new Error(`Invalid configuration: base URL cannot be "health"`);
            }

            if (seenRoutes.has(bank.baseUrl)) {
                throw new Error(`Invalid configuration: duplicate base URL "${bank.baseUrl}"`);
            }

            seenRoutes.add(bank.baseUrl);
        }
    }
}
