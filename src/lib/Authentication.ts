
export interface HttpAuthentication {
    token?: string;
    username?: string;
    password?: string;
}

export function authenticationMiddleware(auth: HttpAuthentication): (req, res, next) => void {
    return (req, res, next) => {
        const headerAuthorization = (req.headers.authorization || "").split(" ");
        const authorizationType = headerAuthorization[0];
        if (authorizationType === "Basic") {
            const b64auth = headerAuthorization[1] || "";
            const authHeaderEntries = Buffer.from(b64auth, "base64").toString().split(":");
            const [username, password] = (authHeaderEntries?.length === 2) ? authHeaderEntries : [null, null];

            if (username === auth.username && password === auth.password) {
                return next();
            }

            res.set("WWW-Authenticate", "Basic realm=\"401\"");
            res.status(401).send("Authentication required.");
        } else if (authorizationType === "Bearer") {
            const token = headerAuthorization[1];

            if (token === auth.token) {
                return next();
            }

            res.set("WWW-Authenticate", "Bearer realm=\"401\"");
            res.status(401).send("Authentication required.");
        } else {
            res.status(401).send("Authentication required.");
        }
    };
}
