
import * as bunyan from "bunyan";
import { Router } from "express";
import * as fs from "fs";
import * as mustache from "mustache";
import { LATENCY_PATH } from "../constants";
import { APPLICATION_VERSION } from "../metadata";

const app: Router = Router();

app.get("/", home);
// app.post(LATENCY_PATH, latency);
app.post(LATENCY_PATH, latency);

export = app;

function home(req, res) {
    res.app.locals.logger.info("/home");

    const response = `
<html>
<body>
<h1>Bank Mocks</h1>
<p><b>Version ${APPLICATION_VERSION}</b></p>
<p>Mock bank implementations for internal use by Transparent Systems.</p>
<p>See <a href="/health"><code>/health</code></a> for uptime and configuration details.</p>
</body>
</html>`;

    res.send(response);

}

const missingRequiredParameterTempl =
    fs.readFileSync(__dirname + "/../templates/latency_missing_required_parameter_response.json").toString();

const logger = bunyan.createLogger({name: "latency configuration"});

function latency(req, res) {
    const requiredParams: string[] = [
        "seconds",
    ];
    let result;
    for (const param of requiredParams) {
        const view = {
            param,
        };
        if (req.body.hasOwnProperty(param)) {
            logger.info(`Received valid latency request: ${JSON.stringify(req.body)}`);
            const {seconds} = req.body;
            globalThis.latency_seconds = seconds;

            res.send(globalThis.latency_seconds);
        } else {
            // Param was not passed in
            logger.info(`Request missing required data: ${param}`);
            result = mustache.render( missingRequiredParameterTempl, view);
            res.status(400).send(JSON.parse(result));
        }
    }
}
