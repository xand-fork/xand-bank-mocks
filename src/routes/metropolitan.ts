import * as bunyan from "bunyan";
import { Router } from "express";
import * as fs from "fs";
import * as jp from "jsonpath";
import * as mustache from "mustache";
import * as validator from "validator";
import { authenticationMiddleware } from "../lib/Authentication";
import Configuration, { BankConfig } from "../lib/Configuration";
import MockBank from "../lib/MockBank";

const logger = bunyan.createLogger({ name: "metropolitan-mock" });
const reserveAcct = 5555555555;

const descriptionMaxLength = 33;

const REQUEST_DENIED =
    fs.readFileSync(__dirname + "/../templates/metropolitan_request_denied.json").toString();

const BALANCE_TEMPLATE =
    fs.readFileSync(__dirname + "/../templates/metropolitan_account_template.json").toString();

const HISTORY_TEMPLATE =
    fs.readFileSync(__dirname + "/../templates/metropolitan_history_template.json").toString();

const TRANSACTION_TEMPLATE =
    fs.readFileSync(
        __dirname + "/../templates/metropolitan_transaction_template.json").toString();

const DESCRIPTION_TOO_LONG_TEMPLATE =
    fs.readFileSync(
        __dirname + "/../templates/metropolitan_desc_too_long.json").toString();

class Transaction {
    public fromAccountId: number = -1;
    public toAccountId: number = -1;
    public amount: number = -1;
    public datetime: string = "";
    public confirmationNumber: number = -1;
    public transferToDescription: string = "";
}

// tslint:disable-next-line:max-classes-per-file
export default class Metropolitan extends MockBank<Transaction, number> {
    constructor(cfg: Configuration, bankConfig: BankConfig) {
        super(cfg, bankConfig);
        this.acctBalances.set(reserveAcct, this.initialReserveBalance);
    }

    public produceRouter(): Router {
        const routes: Router = Router();

        routes.use((req, res, next) => {
            if (req.path === "/getAccessToken") {
                if (this.bankConfig?.authentication) {
                    authenticationMiddleware(this.bankConfig.authentication)(req, res, next);
                } else {
                    next();
                }
            } else {
                authenticationMiddleware({ token: "MOCK_MCB_BEARER_TOKEN" })(req, res, next);
            }
        });

        routes.get("", (req, res, next) => {
            res.send("up");
        });

        // auth endpoint
        routes.post("/getAccessToken", (req, res, next) => {
            res.send({
                access_token: "MOCK_MCB_BEARER_TOKEN",
                token_type: "Bearer",
                expires_in: 86400,
            });
            next();
        });

        // account balance endpoint
        routes.post("/Accounts", (req, res, next) => {
            // validate account id
            const accountIdStr = jp.value(req.body, "$.AcctInqRq.AcctSel.AcctId");
            const accountId = parseInt(accountIdStr, 10);
            if (!accountId || !Number.isInteger(accountId)) {
                res.status(400).send(JSON.parse(REQUEST_DENIED));
                next();
                return;
            }

            // fetch balance for account id
            let rawBalance = this.acctBalances.get(accountId);
            if (rawBalance === undefined) {
                this.acctBalances.set(accountId, this.initialAccountBalance);
                rawBalance = this.initialAccountBalance;
            }

            // convert to currency formatted number
            const balance = rawBalance.toFixed(2);

            const result = mustache.render(BALANCE_TEMPLATE, {
                account_id: accountId,
                balance,
            });
            res.send(JSON.parse(result));
            next();
        });
        routes.post("/Transfers", (req, res, next) => {
            const fromAccountIdStr = jp.value(req.body, "$.XferReq.XferInfo.FromAcctRef.AcctKeys.AcctId");
            const toAccountIdStr = jp.value(req.body, "$.XferReq.XferInfo.ToAcctRef.AcctKeys.AcctId");
            const amount = jp.value(req.body, "$.XferReq.XferInfo.CurAmt.Amt");
            const transferToDescription = jp.value(req.body, "$.XferReq.XferInfo.XferToDesc") as string;

            const fromAccountId = parseInt(fromAccountIdStr, 10);
            const toAccountId = parseInt(toAccountIdStr, 10);

            if (!Number.isInteger(fromAccountId) || !Number.isInteger(toAccountId)
                || !validator.isFloat(amount.toString())) {
                res.status(400).send(JSON.parse(REQUEST_DENIED));
                next();
                return;
            }

            if (transferToDescription.length > descriptionMaxLength) {
                res.status(503).send(JSON.parse(DESCRIPTION_TOO_LONG_TEMPLATE));
                next();
                return;
            }

            if (!this.acctBalances.has(fromAccountId)) {
                this.acctBalances.set(fromAccountId, this.initialAccountBalance);
            }

            if (!this.acctBalances.has(toAccountId)) {
                this.acctBalances.set(toAccountId, this.initialAccountBalance);
            }

            if (this.acctBalances.get(fromAccountId) - amount < 0) {
                logger.warn("Attempted transfer that would result in negative balance", req.body);
                res.status(400).send(JSON.parse(REQUEST_DENIED));
                next();
                return;
            }
            this.acctBalances.set(fromAccountId, this.acctBalances.get(fromAccountId) - amount);
            this.acctBalances.set(toAccountId, this.acctBalances.get(toAccountId) + amount);

            const datetimeStr = generateMcbDatetimeString();
            const tx: Transaction = {
                fromAccountId,
                toAccountId,
                amount,
                datetime: datetimeStr,
                confirmationNumber: this.seenTransactions.length,
                transferToDescription,
            };

            this.seenTransactions.push(tx);
            logger.info("New transfer", tx);

            const response = mustache.render(TRANSACTION_TEMPLATE, {
                from_account_id: fromAccountId,
                confirmation_number: tx.confirmationNumber,
                tx_datetime: tx.datetime,
            });
            res.send(JSON.parse(response));
        });

        // account history endpoint
        routes.post("/AcctTrn", (req, res, next) => {
            const accountId = jp.value(req.body, "$.AcctTrnInqRq.AcctTrnSel.AcctKeys.AcctId");
            const cursor = jp.value(req.body, "$.AcctTrnInqRq.RecCtrlIn.Cursor") || 0;
            const pageSize = 10;

            if (!accountId || !validator.isInt(accountId)) {
                res.status(400).send(JSON.parse(REQUEST_DENIED));
                next();
                return;
            }
            const cursorInt = parseInt(cursor, 10);
            const accountIdInt = parseInt(accountId, 10);
            const allTxns = this.seenTransactions
                .filter((tx) => {
                    return tx.fromAccountId === accountIdInt || tx.toAccountId === accountIdInt;
                })
                .map((tx) => {
                    return {
                        tx_datetime: tx.datetime,
                        tx_num: tx.confirmationNumber,
                        account_id: accountId,
                        tx_type: tx.fromAccountId === accountIdInt ? "Debit" : "Credit",
                        tx_amount: tx.amount,
                        transfer_to_description: tx.transferToDescription,
                        last: false,
                    };
                })
                .reverse();

            const startIndex = cursorInt * pageSize;
            const endIndex = startIndex + pageSize;
            const pageOfTxns = allTxns.slice(startIndex, endIndex);
            const nextCursor = cursorInt + 1;
            if (pageOfTxns.length > 0) {
                pageOfTxns[pageOfTxns.length - 1].last = true;
            }

            const result = mustache.render(HISTORY_TEMPLATE, {
                transactions: pageOfTxns,
                tx_count: allTxns.length,
                cursor: nextCursor,
            });
            const responseJson = JSON.parse(result);

            // Set the cursor value if we have passed the last transaction in the set
            if (endIndex >= allTxns.length) {
                responseJson.AcctTrnInqRs.RecCtrlOut.Cursor = null;
            }

            res.send(responseJson);
        });

        return routes;
    }
}

/**
 * Currently MCB returns a datetime string that has no timezone specified and millisecond precision.
 * Generate a timestamp string that fits that format
 */
function generateMcbDatetimeString(): string {
    // Generate string
    let datetime = new Date().toISOString();
    // Trim trailing "Z"
    datetime = datetime.slice(0, -1);
    return datetime;
}
